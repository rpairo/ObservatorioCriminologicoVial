package tech.rpairo.crimvial.view.activities;

import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import tech.rpairo.crimvial.R;
import tech.rpairo.crimvial.view.fragments.FragmentAutores;
import tech.rpairo.crimvial.view.fragments.FragmentBlog;
import tech.rpairo.crimvial.view.fragments.FragmentCriminologia;
import tech.rpairo.crimvial.view.fragments.FragmentCursos;
import tech.rpairo.crimvial.view.fragments.FragmentEventos;
import tech.rpairo.crimvial.view.fragments.FragmentLibros;
import tech.rpairo.crimvial.view.fragments.FragmentObservatorio;

public class MainActivity extends AppCompatActivity {

    //region Variables
    private DrawerLayout drawerLayout;
    //endregion

    //region Activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        this.agregarToolbar();

        if (navigationView != null) {
            prepararDrawer(navigationView);

            // Seleccionar item por defecto
            seleccionarItem(navigationView.getMenu().getItem(0));
        }
    }
    //endregion

    //region Toolbar

    /**
     * agregarToolbar crea la Toolbar, el DraweLayout y añade el Toggle para el efecto al desplegar el NavigationView
     */

    private void agregarToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

            this.drawerLayout.setDrawerListener(toggle);
            toggle.syncState();

        }
    }
    //endregion

    // region Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion

    // region NavigationView
    private void prepararDrawer(NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        seleccionarItem(menuItem);
                        drawerLayout.closeDrawers();
                        return true;
                    }
                });
    }

    private void seleccionarItem(MenuItem itemDrawer) {
        Fragment fragmentoGenerico = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (itemDrawer.getItemId()) {
            case R.id.item_obser:
                fragmentoGenerico = new FragmentObservatorio();
                break;
            case R.id.item_blog:
                fragmentoGenerico = new FragmentBlog();
                break;
            case R.id.item_eventos:
                fragmentoGenerico = new FragmentEventos();
                break;
            case R.id.item_cursos:
                fragmentoGenerico = new FragmentCursos();
                break;
            case R.id.item_libros:
                fragmentoGenerico = new FragmentLibros();
                break;
            case R.id.item_crim:
                fragmentoGenerico = new FragmentCriminologia();
                break;
            case R.id.item_autores:
                fragmentoGenerico = new FragmentAutores();
                break;
        }
        if (fragmentoGenerico != null) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, fragmentoGenerico)
                    .commit();
        }

        // Setear título actual
        setTitle(itemDrawer.getTitle());
    }
    //endregion
}