package tech.rpairo.crimvial.view.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tech.rpairo.crimvial.R;

public class FragmentAutores extends Fragment {

    //region Constructor
    public FragmentAutores() {
    }
    //endregion

    //region Fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_autores, container, false);

        return view;
    }
    //endregion
}